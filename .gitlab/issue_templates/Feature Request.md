<!-- Summarize your feature here. What are you trying to achieve? How do you want HPCToolkit to help you achieve that? -->

### Rationale

<!-- Please detail any rationale for adding this feature here. Why should HPCToolkit support this feature? Who would use this feature? -->

### Description

<!-- Describe what the feature should actually do. Do you have a mock-up command sequence or screenshot? Have you considered any alternatives? -->

### Additional Information

<!-- If you have any other context about the feature request, please write that here. Do you develop an application that would be assisted by this feature? Tell us about it! -->

<!-- Do not remove the following line. -->

/label ~"type::feature"
