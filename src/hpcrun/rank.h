// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef _HPCRUN_RANK_H_
#define _HPCRUN_RANK_H_

int hpcrun_get_rank(void);

#endif
