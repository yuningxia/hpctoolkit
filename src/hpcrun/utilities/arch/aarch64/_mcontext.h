// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef __MCONTEXT_H
#define __MCONTEXT_H

#include <ucontext.h>
#include <sys/ucontext.h>

#endif
